'use strict';

const firebase = require('../db');
const Employe = require('../models/employe.js');
const Emails = require('../models/emails.js');
const firestore = firebase.firestore();

// create an employe
const addEmploye = async (req, res, next) => {
    try {
        const data = req.body;
        const email = await createEmail(data);
        data.email = email;
        await firestore.collection('employes').doc().set(data);
        res.send('el empleado se guardó correctamente');
    } catch (error) {
        res.status(400).send(error.massage);
    }
}

async function createEmail(params){
    let domain = ""
    let complemail = "";
    let localcounter = 0;
    if (params.pais === "Colombia"){
        domain = "@cidenet.com.co"
    } else if (params.pais === "Estados Unidos") {
        domain = "@cidenet.com.us"
    }
    let pseudoemail = params.pnombre + '.' + params.papellido + domain;
    try {
        const emails = await firestore.collection('emails');
        const emailobj = await emails.where("pseudoemail", "==", pseudoemail).get();
        const alldata = [];
        emailobj.forEach(doc => {
            const dataEmail = new Emails(
                doc.id,
                doc.data().pseudoemail,
                doc.data().counter
            );
            alldata.push(dataEmail);
          });
        if (emailobj.empty){
            firestore.collection('emails').doc(pseudoemail).set({
                "pseudoemail": pseudoemail,
                "counter": 0
            });
            return pseudoemail;
        } else {
            localcounter = alldata[0].counter + 1;
            complemail = params.pnombre + "." + params.papellido + "." + localcounter + domain;
            await firestore.collection('emails').doc(pseudoemail).set({
                "pseudoemail": pseudoemail,
                "counter": localcounter
            });
            return complemail;
        }
    } catch(error) {
        res.status(400).send(error.massage);
    }
};

// get all employes
const getAllEmployes = async (req, res, next) => {
    try {
        const employes = await firestore.collection('employes');
        const data = await employes.get();
        const allEmployes = [];
        if(data.empty) {
            res.status(404).send('No se encuentran empleados');
        } else {
            data.forEach(doc => {
                const employe = new Employe(
                    doc.id,
                    doc.data().pnombre,
                    doc.data().snombre,
                    doc.data().papellido,
                    doc.data().sapellido,
                    doc.data().typid,
                    doc.data().numid,
                    doc.data().pais,
                    doc.data().area,
                    doc.data().email,
                    doc.data().estado,
                    doc.data().ingreso,
                    doc.data().registro
                );
                allEmployes.push(employe);
            });
            res.send(allEmployes);
        }
    } catch (error) {
        res.status(404).send(error.massage);
    }
}

//get employe by id
const getaEmploye = async (req, res, next) => {
    try {
        const id = req.params.id;
        const employe = await firestore.collection('employes').doc(id);
        const data = await employe.get();
        if(!data.exists) {
            res.status(404).send('el empleado con ese id no se encuentra');
        }else {
            res.send(data.data());
        }
    } catch (error) {
        res.status(400).send(error.message);
    }
}

//update an employe
const updateEmploye = async (req, res, next) => {
    let newEmail = "";
    try {
        const id = req.params.id;
        let data = req.body;
        const employe =  await firestore.collection('employes').doc(id);
        const fireData = await employe.get();
        if (fireData.data().pnombre === data.pnombre && fireData.data().papellido === data.papellido){
            await employe.update(data);
            res.send('empleado actualizado correctamente');
        } else {
            newEmail = await createEmail(data);
            data.email = newEmail;
            await employe.update(data);
            res.send('empleado actualizado correctamente y su correo se actualizo a: ' + data.email);
        }  
    } catch (error) {
        res.status(400).send(error.message);
    }
}

//validate names and lastname to update
async function validateData(params) {

}

//delete an employe
const deleteEmploye = async (req, res, next) => {
    try {
        const id = req.params.id;
        await firestore.collection('employes').doc(id).delete();
        res.send('empleado eliminado');
    } catch (error) {
        res.status(400).send(error.message);
    }
}
module.exports = {
    addEmploye,
    getAllEmployes,
    getaEmploye,
    updateEmploye,
    deleteEmploye
}
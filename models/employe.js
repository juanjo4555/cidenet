class Employe {
    constructor(id, pnombre, snombre, papellido, sapellido, typid, numid, pais, area, email, estado, ingreso, registro){
        this.id = id,
        this.pnombre = pnombre,
        this.snombre = snombre,
        this.papellido = papellido,
        this.sapellido = sapellido,
        this.typid = typid,
        this.numid = numid,
        this.pais = pais,
        this.area = area,
        this.email = email,
        this.estado = estado,
        this.ingreso = ingreso,
        this.registro = registro
    }
}

module.exports = Employe;
const express =require('express');
const {addEmploye, getAllEmployes, getaEmploye, updateEmploye, deleteEmploye} = require('../controllers/employeController');

const router = express.Router();

router.post('/employe', addEmploye);
router.get('/employe', getAllEmployes);
router.get('/employe/:id', getaEmploye);
router.put('/employe/:id', updateEmploye);
router.delete('/employe/:id', deleteEmploye);

module.exports = {
    routes: router
}